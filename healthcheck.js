const fs = require('fs');
const path = require('path');
const cron = require('node-cron');

const log = (code, message) => {
  // Check log level
  if (code < logLevel) {
    return;
  }

  // Get time and create timestamp string
  const now = new Date();
  const currentTimestamp = `[${String(now.getFullYear()).padStart(2, '0')}-${String(now.getMonth() + 1).padStart(2, '0')}-${String(now.getDay()).padStart(2, '0')}] [${String(now.getHours()).padStart(2, '0')}:${String(now.getMinutes()).padStart(2, '0')}:${String(now.getSeconds()).padStart(2, '0')}]`;

  // Create barebone log message to append to
  let logMessage = `${currentTimestamp}`;

  // Append type to log message
  switch(code) {
    // Debug
    case 0:
      logMessage += ' [DEB]';
      break;
    // Info
    case 1:
      logMessage += ' [NFO]';
      break;
    // Critical
    case 2:
      logMessage += ' [CRT]';
      break;
    // Error
    case 3:
      logMessage += ' [ERR]';
      break;
    default:
      log(3, `Unable to log "${message}": Unknown log code "${code}"`);
      return;
  }

  // Append message to log message
  logMessage += ` ${message}`;

  // Print log message
  switch(code) {
    // Debug
    case 0:
      console.debug(logMessage);
      break;
    // Info
    case 1:
      console.info(logMessage);
      break;
    // Critical
    case 2:
      console.warn(logMessage);
      break;
    // Error
    case 3:
      console.error(logMessage);
      break;
    default:
      log(3, `Unable to log "${message}": Unknown log code "${code}"`);
      return;
  }
}

let logLevel = 0;

log(1, `Script started`);

cron.schedule("*/5 * * * *", () => fs.readFile(path.resolve(__dirname, 'config/config.json'), 'utf8', async (err, data) => {
  if (err) {
    console.error(`Error while fetching data: ${err}!`);
    return;
  }

  // Get and parse json
  const config = JSON.parse(data);

  // Update log level
  logLevel = config.logLevel;

  // Check for any checks
  if (config.checks.length < 1) {
    log(0, 3, 'No checks have been configured');
    return 1;
  }

  // Perform checks
  log(1, `Performing ${config.checks.length} checks`);

  for (const check of config.checks) {
    try {
      log(0, `Checking ${check.scheme}://${check.host}${check.path}`);
      const checkRes = await fetch(`${check.scheme}://${check.host}${check.path}`, {
        method: 'GET', redirect: 'follow'
      })

      if (checkRes.ok) {
        log(0, `Check of ${check.host} successful with final url of ${checkRes.url}!`);

        try {
          log(0, `Pinging heartbeat endpoint for ${check.host}...`)
          const hcRes = await fetch(`${config.checkHost}${check.checkId}`);
          if (hcRes.ok) {
            log(1, `Uptime update of ${check.host} successful!`);
          } else {
            log(3, `Heartbeat endpoint ping to ${config.checkHost}${check.checkId} of host ${check.host} failed with status ${hcRes.status}: ${hcRes.statusText}`);
          }
        } catch(err) {
          log(3, `Heartbeat endpoint ping to ${config.checkHost}${check.checkId} of host ${check.host} failed with an error: ${err}`);
        }
      } else {
        log(2, `Check of ${check.host} failed at ${checkRes.url} with status ${checkRes.status}: ${checkRes.statusText}`);
      }
    } catch(err) {
      log(3, `Check of ${check.host} failed with an error: ${err}`);
    }
  }
  log(1, `Uptime update done.`);
}))